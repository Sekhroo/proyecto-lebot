//Constantes inmodificables
const Discord = require("discord.js");
const client = new Discord.Client();

//Constante enlace con config.json (Puede modificar prefijo y token)
const config = require("./config.json");

//Funcion prensencia (Que esta haciendo y su estado)
function presence(){
    client.user.setPresence({
        status:"online",
        activity: {
            name: "mejorar Chile",
            type: "PLAYING"
        }
    })
}

//Activa al bot (modo online en el server)
client.on("ready",() => {
    console.log("Hola mundo");
    presence();
});

//Llama a la funcion prefijo
let prefix = config.prefix;

//Funciones del bot
client.on("message", (message) => {

    //No responde si el comando no tiene prefijo o si lo llama un bot
    if(message.author.bot) return;
    if(!message.content.startsWith(prefix)) return;

    //Lee el prefijo (caracteres que llaman al bot)
    const args = message.content.slice(prefix.length).trim().split(/ +/g);
    const command = args.shift().toLocaleLowerCase();

// Acciones segun comandos
if (command === "mejordoctrina") {
    message.channel.send("Comunismo: Doctrina económica, política y social que defiende una organización social en la que no existe la propiedad privada ni la diferencia de clases, y en la que los medios de producción estarían en manos del Estado, que distribuiría los bienes de manera equitativa y según las necesidades.");
}else
if (command === "uwu") {
    message.channel.send("no porfa, para");
}else
if (command === "tai") {
    message.channel.send("si toy");
}else
if (command === "buena") {
    message.channel.send("a");
}
});

client.on("message", message =>{
    
    if(message.author.bot) return;
    if (!message.content.startsWith(prefix)) return;

    const args = message.content.slice(prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase(); 

if(command === 'momazo'){
    const embed = new Discord.MessageEmbed()
        .setTitle('CACHA ESTE MOMAZO XDDDDDDDDDDDDD')
        //.addField('name', message.guild.name, true)
        //.addField('Integrantes', message.guild.memberCount, true)
        //.setAuthor(client.user.username, client.user.avatarURL())
        .setImage('https://i.pinimg.com/originals/54/cb/bb/54cbbb35a38e5ceb35b9849e386c80d6.jpg')
        //.setURL('https://gitlab.com/Sekhroo/proyecto-lebot.git');
    message.channel.send(embed);


}
if(command === 'meme'){
    const image = new Discord.MessageAttachment('https://images3.memedroid.com/images/UPLOADED144/5ef0f22a83a4e.jpeg');
    message.channel.send(image);
}
});
  
//Token identifica al bot
client.login(config.token);